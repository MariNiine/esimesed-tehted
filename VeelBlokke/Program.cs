﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeelBlokke
{
    class Program
    {
        static void Main(string[] args)
        {
            //SWITCH - keerulisem, aga mugavam

            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    //laused mida täidetakse laupäeval
                    Console.WriteLine("Täna saab sauna");
                    //break;
                    goto case DayOfWeek.Sunday;
                case DayOfWeek.Sunday:
                    //laused mida täidetakse pühapäeval
                    Console.WriteLine("Joome õlutit");
                    break;
                case DayOfWeek.Wednesday:
                    //laused mida täidetakse kolmapäeval
                    Console.WriteLine("Lähen palli mängima");
                    break;
                default:
                    //laused mida täidetakse muudel päevadel
                    Console.WriteLine("tööle-tööle kurekesed");
                    break;
            }
            Console.Write("Press hani kii ...");
            Console.ReadKey();

        }
    }
}
