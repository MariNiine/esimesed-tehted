﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReedeHommik1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Abiline, mida järgnevates ülesannetes vaja läheb

          /* Random r = new Random(); // see on juhusliku arvu genereerija andmetüüp

            // NB! kui selle randomi tegemisel anda ette mingi number, siis need juhuslikud
            // asjad on iga kord juhuslikult samad

            #region region proovimiseks
            int proov = r.Next(); // Randomi käest saab küsida juhuslikke täisarve
            Console.WriteLine(i);

            proov = r.Next(100); // juhuslikke arve kuni sajani (excl)
            Console.WriteLine(i);

            proov = r.Next(10, 20); // juhuslikke arve 10-st (included) 20ni (excluded)
            Console.WriteLine(i);

            double d = r.NextDouble(); // juhuslikke double-id vahemikus  0 kuni 1 (excl)
            Console.WriteLine(d);
            #endregion*/


            // 1. tee täisarvu massiiv - 100 tk
            // 2. täida see juhuslike arvudega
            // 3. leia sealt massiivist (ja ütle ekraanile) kõige suurem, kõige väiksem, keskmine

            Random r = new Random();
            int[] arvud = new int[80]; 
            for (int i = 0; i < arvud.Length; i++)
            {
                arvud[i] = r.Next(1000);
            }

            for (int i = 0; 1 < arvud.Length; i++)
            {
             
            //    Console.Write(arvud[i]);
            //    Console.WriteLine("\t");
            //if (i % 10 == 9) Console.WriteLine();

          //  Console.Write($"{arvud[i]}{(i % 10 == 9 ? "\n" : "\t")} ");
            }

        int suurim = arvud[0];
            int väikseim = arvud[0];
            int summa = 0;

            for (int i = 1; i < arvud.Length; i++)
            {
                suurim = arvud[i] > suurim ? arvud[i] : suurim;
                väikseim = arvud[i] < väikseim ? arvud[i] : väikseim;
                summa += arvud[i];

                Console.WriteLine($"suurim on: {suurim}");
                Console.WriteLine($"väikseim on: {väikseim}");
                Console.WriteLine($"keskmine on: {summa / arvud.Length}");
            }

            Console.WriteLine("suurim on: "+ suurim.ToString());
            //see on tavaline stringi liitmine - lihtne aru saada, ébamugav kirjutada ja lugeda
            //stringi liitmise teehe ei ole alati kõige viisakam
            Console.WriteLine("väikseim on: {0}", väikseim);
            //siin kasutame placeholderiga {}
            //kohta vahele prinditavad või kuvatavad väärtused
            //pisut parem ja unoversaalne
            Console.WriteLine($"keskmine on: {summa / arvud.Length}");
            //siin kasutame interpoleeritud stringi - $string
            //kus placeholderite sissekirjutatakse väärtus, mida kuvada-printida

            // siit hakkab uus harjutus
            Console.WriteLine("\nHarjutus 2\n");

            int variant = r.Next();
            Random uusR = new Random(variant);

            int[,] table = new int[10, 10];
            for (int i = 0; i < table.GetLength(0); i++)
            {
                for (int j = 0; j < table.GetLength(1); i++)
                {
                    table[i, j] = uusR.Next(100);
                    Console.Write(table[i, j] + "\t");
                }
                Console.WriteLine();
            }

            //variant massiivide massiiviga
            int otsitav = -1;
            do
            {
                Console.Write("mida otsime (0...100)");
                if (int.TryParse(Console.ReadLine(), out otsitav)
                    && otsitav >= 0
                    && otsitav < 100
                    ) break;
                Console.WriteLine("kas pole õige suurusega või pole arv");
            } while (true);
            Console.WriteLine($"otsime {otsitav}");


            bool leidsime = false;
            for (int i = 0; i < table.GetLength(0); i++)
                for(int j = 0; j < table.GetLength(1); i++)
                if (table[i, j] == otsitav)

                {

                    leidsime = true;
                    Console.WriteLine($"leidsin {otsitav} reast {i+1} ja veerust {j+1}");
                }
            if (!leidsime) Console.WriteLine($"arvu {otsitav} meie tabelis ei ole");
        


    }
    }
}
