﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SpordipaevaTask2
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"C:\Users\marin_m3sdpn5\Source\Repos\Esimesed-tehted\SpordipaevaTask2\TextFile1.txt"; //loen faili sisse
            var sisu = File.ReadAllLines(filename);     //loe kõik read sisse (ReadAllLines annab alati string array)
            Console.WriteLine(string.Join("\n", sisu)); //kontrollin, kas sai sisse loetud

            //Ma pean read sisse lugema, siis kuhugi paema, jaotama distantside kaupa.

            //Loen ühte listi, siis vaatab, mis edasi
            Dictionary<int, List<(string, double)>> DistantsideKiirused = new Dictionary<int, List<(string, double)>>(); //võtmeks on distants, sisuks on list. Tegemist kahemõõtmelise massiiviga
            // Saan listi, mille sisu on nimi ja kiirus
            // 1 - distants, 2 - distantsil olevad kiirused

            for (int i = 1; i < sisu.Length; i++) // täidan dictionary. Esimese
            {
                var rida = sisu[i].Replace(",", ",").Split(','); //esimese rea
                string nimi = rida[0]; // nimi esimene
                int distants = int.Parse(rida[1]); // distants tene
                double kiirus = distants / double.Parse(rida[2]); // arvutan kohe kiiruse välja
                //tekitas ühe rea peale nimi, kiirus, distants

                //Kas selles dictionarys on distantsi jaoks olemas rida? Kui pole, siis teeme. Võtmeks distants ja teeme ühe tühja listi.
                if (!DistantsideKiirused.ContainsKey(distants)) DistantsideKiirused.Add(distants, new List<(string, double)>()); // 
                DistantsideKiirused[distants].Add((nimi, kiirus)); // See on tühi distantsi list algu. Lisan sinna sisu read - nimi, kiirus
            }
                foreach(var x in DistantsideKiirused)
            { 
                    Console.WriteLine($"Distantsil {x.Key} mõõdeti {DistantsideKiirused[x.Key].Count} aega"); // Distantsi tulemuste list ja count ütleb palju neid oli. Mis on x?
                foreach (var y in x.Value) Console.WriteLine("\t" + y);
            }
                //leia iga distantsi kiireim
                foreach (var x in DistantsideKiirused)
            {
                //x.Key on distants ja x.Value on list, kust on vaja leida kiireim
                int kiireim = 0;
                for(int i = 1; i < x.Value.Count; i++) //võrdlen
                
                {
                 if (x.Value[i].Item2 > x.Value[kiireim].Item2) kiireim = i; //pean võrdlema kiiruseid, mitte paare. Võrdlen jooksev rida kiirus ja kiireim kiirus.
                }
                Console.WriteLine($"Distantsil {x.Key} oli kiireim {x.Value[kiireim].Item1}");

             
            }

            //List<string> nimed = new List<string>();
            //List<int> distants = new List<int>();

    

          //  for (int i = 0; i < 20; i++)
          {
               // Console.WriteLine($");
                    }

          //  List<int> kiireim = new List<int> { };
            {

            }
        }
    }
}
