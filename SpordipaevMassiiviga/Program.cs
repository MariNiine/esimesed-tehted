﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SpordipaevMassiiviga
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"C:\Users\marin_m3sdpn5\Source\Repos\Esimesed-tehted\SpordipaevMassiiviga\TextFile1.txt";

            string[] sisu = File.ReadAllLines(filename); // 1. Loen siss. tulemus massiivi ridadega asi

            int distantse = 0; //6. eeldan, et nulliga kiirust ei ole
            int[] distantsid = new int[sisu.Length]; //2. teen distantside massiivi. Vähemalt nii pikk, kui massiivis ridu
            for (int i = 1; i < sisu.Length; i++) //3. käin faili läbi
            {
                int distants = int.Parse(sisu[i].Split(',')[1]); // 4. võtan sealt distantsid
                if (!distantsid.Contains(distants)) { distantsid[distantse++] = distants; } // 5. panen massiivi kirja. Kui selline distans on olemas, siis ei tee midagi.

                // Lause selgitus {distantsid[distantse++] = distants;} - pannakse massiivi kirja ja suurendab ühe võtta distantside arvu pärast. 
            }
            // selleks hetkeks on mul failis kirjas kõik erinevad distantsid

            for (int i = 0; i < distantse; i++)
            {
                Console.WriteLine(distantsid[i]);
                double suurimKiirus = 0;
                string kiireimJooksja = "";
                for (int j = 1; j < sisu.Length; j++)
                {
                    var rida = sisu[j].Split(',');
                    int distants = int.Parse(rida[1]);
                    string nimi = rida[0];
                    double kiirus = distants / double.Parse(rida[2]);
                    if (distants == distantsid[i])
                    {
                        if (suurimKiirus == 0 || kiirus > suurimKiirus) { suurimKiirus = kiirus; kiireimJooksja = nimi; }
                    }

                    Console.WriteLine($"Distantsil {distantsid[i]} oli kiireim {kiireimJooksja}");
                }
            }
        }
    }
}
