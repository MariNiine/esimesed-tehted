﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;  //siia lisa!

namespace TaiendavadTykid
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"c: \andmed\Uus fail.txt";
            string filename2 = @"\..\..\Uus fail.txt"; // kaks punkti tähendab kaust korrus kõrgemale
            string filename3 = @"C:\Users\marin_m3sdpn5\source\repos\Esimesed-tehted\TaiendavadTykid\Uus fail.txt";

            string sisu = File.ReadAllText(filename3);
            Console.WriteLine(sisu);

            string[] loetudRead = File.ReadAllLines(filename3);
            for (int i = 0; 1 < loetudRead.Length; i++)
            {
                Console.WriteLine($"rida {i} - {loetudRead[i]}");
            }

            int[] arvud = { 1, 2, 3, 4, 5 };
            Console.WriteLine(string.Join(",", arvud));

            var nimed = File.ReadAllLines(@"C:\Users\marin_m3sdpn5\source\repos\Esimesed-tehted\TaiendavadTykid\Uus fail.txt");
            foreach (var x in nimed) Console.WriteLine(x.Split(' ')[0]);

        }
    }
}
