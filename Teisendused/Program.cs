﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Teisendused
{
    class Program
    {
        static void Main(string[] args)
        {

            //aegajalt on andmetüüpe vaja ühest teiseks teisendada või ka ühest teiseks teisendada
            // mis on var

            #region Kommenteerisin välja (region & endregion)
            //var ilusMuutuja = 7D; // muutuja ilusMuutuja on selleks, et teda uurida
            //Console.WriteLine(ilusMuutuja.GetType().Name);
            //Console.WriteLine("ilusMuutuja väärtus on {0}, ilusMuutuja);
            //var teine = ilusMuutuja;

            //var mari = new { Nimi = "Mari", Vanus = 28 };
            //Console.WriteLine(mari.GetType().Name);

            //foreach (var x in mari.GetType().GetProperties()) Console.WriteLine(x.Name);
            #endregion

            //Arvude teisendamine teisteks arvudeks

            int i = 1000;
            long l = i; // toimub ilmutamata tüübiteisendus

            i = (int)l; //ilmutatud tüübiteisendus

            i = (int)l + 1 * 2; // ilmutatud tüübiteisendused e casting
            // casting on suhteliselt kõrge prioriteediga, seepärast avaldis sulgudesse

            l = long.MaxValue; // suurim Long arv
            i = (int)l;
            Console.WriteLine(i);

            i = int.MaxValue;
            i++;
            Console.WriteLine(i);

            double d = 10.0;
            i = (int)d;

            // samaliigilisi arve saab teisendada ilmutamata väiksemast suuremasse
            // suuremast väiksemasse või liikide vahel tuleb castida (ilmutult teisendada)

            d = i; // integeri saab omistada kõigile
            decimal dets = i;
            d = (float)dets;

            string tekst = "100";
            //i = (int)tekst; //stringi ei saa castida teistesse tüüpidesse
            // tekst = d; // see ka ei toimi

            // KÕIKI ASJU SAAB TEHA STRINGIKS

            tekst = d.ToString(); // kõigil ajadel on olemas viis, kuidas neid Stringiks tehakse
            Console.WriteLine(tekst);

            i = int.Parse("10"); // see teisedab teksti int asjaks
            Console.WriteLine(i * 2);
            DateTime dt = DateTime.Parse("March 7 1955", new CultureInfo("en-us"));
            Console.WriteLine(dt);

            d = double.Parse("14.0", new CultureInfo("en-us"));

            //tryParse võimaldab proovida ja otsustada 

            Console.Write("Millal oled sa sündinud ");
            string sünniaeg = Console.ReadLine();
            DateTime sünnipäev = DateTime.Parse(sünniaeg);
            Console.WriteLine($"sa siis oled sündinud sellisel päeval {sünnipäev.DayOfWeek}");

            if (DateTime.TryParse(sünniaeg, out DateTime sündinud)) // proovime, kas õnnestub, kui õnnestub kirjutab "sündinud" muutuja sisse. Kui ei õnnestu annab "see pole miski sünniaeg"
            {
                Console.WriteLine("sinu sünnipäev on siis " + sündinud.ToShortDateString());
            }

            else Console.WriteLine("see pole miski sünniaeg");

            /*Elvise variant
            Console.WriteLine
                (DateTime.TryParse(sünniaeg, out DateTime x) ? $"sa oled siis sündinud {x}" : "see pole mingi kuupäev");*/


        }
    }
}
