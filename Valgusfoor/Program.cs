﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valgusfoor
{
    class Program
    {
        static void Main(string[] args)
        {

            //VALGUSFOOR
            // IF variant

            Console.Write("Mis värvi tuli on valgusfooris?\n");
            string varv = Console.ReadLine();

            if (varv == "Red")
            { Console.WriteLine("Ära mine"); }

            else if (varv == "Green")
            { Console.WriteLine("Mine edasi"); }

            else if (varv == "Yellow")
            { Console.WriteLine("Oota"); }

            else
            { Console.WriteLine("Pole sellist värvi\n"); }


            //SWITCH variant

            Console.Write("Mis värvi tuli on valgusfooris?\n");
            switch (Console.ReadLine().ToLower())
            {
                case "green":
                    Console.WriteLine("Mine edasi");
                    break;
                case "yellow":
                    Console.WriteLine("Oota");
                    break;
                case "red":
                    Console.WriteLine("Ära mine");
                    break;
                default:
                    Console.WriteLine("Sellist värvi pole");
                    break;
            }

            Console.Write("Mis kohvimasinal viga on? Kirjuta errori number\n");
            switch (Console.ReadLine().ToLower())
            {
                case "1":
                    Console.WriteLine("Pane piima juurde");
                    break;
                case "2":
                    Console.WriteLine("Katki! Kutsu tehnik");
                    break;
                case "3":
                    Console.WriteLine("Kohvioad otsas");
                    break;
                default:
                    Console.WriteLine("Kõik on korras");
                    break;
            }


            //ELVISe variant
            Console.Write("\n Mis värvi sa näed: ");
            string varve = Console.ReadLine().ToLower();
            Console.WriteLine(
                varve == "green" ? "sõida edasi" : 
                varve == "red" ? "jää seisma" :
                varve == "yellow" ? "oota rohelist" : 
                "pese silmad"
                );


            //switch easy
            /*Console.WriteLine(Console.ReadLine().ToLower() switch 
                "green" => "sõida edasi",
                "red" => "jää seisma",
                "yellow" => "oota rohelist",
                _ => "pese silmad puhtaks"
            );*/

   
        }
    }
}
