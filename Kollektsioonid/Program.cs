﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kollektsioonid
{
    class Program
    {
        static void Main(string[] args)
        {
        // foreach = read only; for tsükkel = saab muuta

            int[] arvud = new int [7] { 1, 5, 3, 8, 9, 2, 3 }; // massiivi kirjapilt
            foreach (int arv in arvud) Console.WriteLine(arv); 
          //  Console.WriteLine(arvud[3]); // trükib välja neljanda numbri reast ehk 8 võimalus // pöörduda konkreetse elemendi poole massiivis
            arvud[3]++;

            List<int> arvud2 = new List<int> { 3, 4, 5, 6, 7, 8, 9 }; //Tegemist on listiga, mis koosneb int-idest. Nokksulgude vahel tüübi tähis. Viitab sellele, et list on geneeriline asi.
                                                                     //list võib koosneda ükskõik millest, ka stringist, massiividest jne. kirjaviisiks on teine kuju kui int. 
                                                                    
            foreach (int arv in arvud2) Console.WriteLine(arv);
            Console.WriteLine(arvud2[4]); // trükib 7 välja
            arvud2[3]++; // liitsime kuuele ühe juurde 6+1 = 7

            arvud2.Add(17); // erinevalt massiivist on mul võimalik juurde lisada asju
            arvud2.Remove(8); //võimalik on välja võtta asju - võtsin välja 8
            arvud2.RemoveAt(2); // kustuta ära teine element

            foreach (var arv in arvud2) Console.WriteLine(arv); // 3, 4, 7, 7, 9, 17

            while (arvud2.Contains(8)) arvud2.Remove(8); // contains = sisaldab

            List<int> arvud3 = new List<int>(); 
           // või
           // List<int> arvud3 = new List<int>(100);// Kui umbes tean kui suur massiiv tukeb võin sulgudesse ette öelda numbri, kui suurt massiivi tahan, võib ka mitte panna
         
            //arvud3.Add(8);
            //arvud3.Add(3);
            //arvud3.Add(4);
            //arvud3.Add(5);
            //arvud3.Add(6); //jne.

            //Tegelikult on List Massiiv. List on massiiv, mis peab meeles, mitu elementi seal on. 
            //Kui esimest korda lisatakse listi element, siis jäetakse meelde, et seal on üks element rohkem.
            //Kui list on täis, siis tehakse automaatselt operatsioon, et tehakse uus list (massiiv), mis on eelmisest 2 korda suurem. 
            //Vanast listist tõstetakse kõik uude ja vana unsutatakse.

            // Listil pole sellist asja nagu Lenght

            for (int i = 0; i < 20; i++) // listil on lenght asemel count
            {
                arvud3.Add(i * i);
                Console.WriteLine($"maht: {arvud3.Capacity} suurus: {arvud3.Count}");
            }

            arvud3 = arvud.ToList(); // massiivi teisendada listiks
            arvud = arvud3.ToArray(); //listi teisendada massiiviks

            List<string> nimekiri2 = new List<string> { "Mari", "Henn", "Margit" };

            int[,] tabel = { { 1, 2, 3 }, {9,8,7 }}; // kahemõõtmeline (kahemõõtmelisel massiivil on ainult 1 lenght
                                                     //Lenght = 6
                                                     //Rnak () = 2
                                                     //GetLenght(0) = 2
                                                     //GetLenght(1) = 3

            int[][] suur // massiiv, mis koosneb massiividest
            = new int[][] { new int[] { 1, 2, 3 }, new int[] { 9, 8, 7 } };
            // suur.Lenght = 2
            // suur[0].Lenght = 3
            // suur[1].Lenght = 4

            List<List<int>> listideList = new List<List<int>>(); 
            listideList.Add(new List<int> { 1, 2, 3 }); // see on listide list.

            // Massiiv - array
            // T [] muutuja = new T [suurus];
            // List <T> muutuja = new List<T>()
            // Massiivil on olemas muutuja [indeks] ja Lenght
            // Massiivil on [,] kahemõõtmeline massiiv ja [][] massiiv, mis koosneb kahest massiivist

            //Listil on muutuja [index] ja Count ja Capasity ja 
            //Add (7) - lisab listi lõppu number 7
            // Remove (8) - eemaldab esimese number 8
            //RemoveAt (3) - eemaldab kolmandalt kohalt elemendi
            // kahemõõtmelist ei ole, on List<List<T>> list, mis koosneb kahest listist

            //Queue<int>; // järjekord
            //Enque(7); // lisab queue Lõppu 7
            //    int x = järjekord.Deque() // loeb algusest

            //Stack<int> - kuhi
            //Push(7) // lisab kuhja peale 
            // 



            // DICTIONARY
            //On struktuur, mis koosneb paaridest - [key, value] ehk [võti, väärtus]
            // .ContainsKey $ Contains.Value
            //Võtmeväärtus peab olema unikaalne

            //foreach (var x in muutuja) - annab paarid [võti, value]
            //foreach (var x in muutuja.Keys) - annab võtmed
            //foreach (var x in muutuja.Values) - annab valued

            Dictionary<int, string> nimekiri = new Dictionary<int, string>
            {
                {1, "Henn" },
                {2, "Ants" },
                {3, "Peeter" },
            };

            nimekiri.Add(7, "Joosep"); //lisasin, kuidas näeks välja
            nimekiri[2] = "Antsuke"; // muutmine indeksi kaudu
            nimekiri[9] = "Pilleke"; // lisamine indeksi kaudu
            Console.WriteLine(nimekiri[2]);

            foreach (var x in nimekiri) Console.WriteLine(x); // käi läbi kogu nimekiri


            Dictionary<string, double> Tulemused = new Dictionary<string, double>
            {
                { "Hiir", 1.111},
                { "Jänes", 200 / 6.0},
          
            };
            Console.WriteLine(Tulemused["Jänes"]);

            //foreach (var k in Tulemused.Keys)
            //{
            //    Tulemused[k] *= 2; // Teeme kõik tulemused kaks korda suuremaks // ei saa teha!
            //    Console.WriteLine(k);
            //}

            foreach (var k in Tulemused.Keys.ToList())
            { 
                Tulemused[k] *= 2;  
            }
            Console.WriteLine(Tulemused["Hiir"]);

            //SORTEERITUD LISTID JA DICTIONARID
            // T on muutuja
            // SortedDictionary<Tkey, Tvalue> - nagu dictionary, aga on sorteeritud. Hoiab võtmete järjekorras dictionary
            // SortedSet<T> - nagu list, aga sorteritud järjekorras. Hoiab listi pandud asju järjekorras.
            // SortedList<Tkey, Tvalue> - nagu dictionary, aga hoitakse sorteerituna
            // Dictionary<Tkey, Tvalue> - nagu dictionary, aga ei hoita sorteerituna (hash-tabelit)
        }
    }
}
