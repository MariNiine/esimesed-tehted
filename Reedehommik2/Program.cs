﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reedehommik2
{
    class Program
    {
        static void Main(string[] args)
        {
            // F12 leiab unassigned variable

            int punkte = 50; // määran  
            Random r = new Random();
                for (int i = 0; i < 10; i++) // 10 tehet tehakse
                {
                    int vastus;     // ütleb, mis tüüpi väärtus on
                    string tehe;    // ütleb, mis tüüpi väärtus on
                                   
                // kaks arvu ja tehe
                int yks = r.Next(1, 10);
                int kaks = r.Next(1, 10);
                int t = r.Next(4);

                (vastus, tehe) =
                    t == 0 ? (yks + kaks, "summa") :
                    t == 1 ? (yks - kaks, "vahe") :
                    t == 2 ? (yks * kaks, "korrutis") :
                    t == 3 ? (yks / kaks, "jagatis") :
                    (0, "");

                 /*   switch (r.Next(4))
                    {
                        case 0:
                            tehe = "summa";
                            vastus = yks + kaks;

                            Console.WriteLine("pluss");
                            break;
                        case 1:
                            tehe = "vahe";
                            vastus = yks - kaks;

                            break;
                        case 2:
                            tehe = "korruta";
                            vastus = yks * kaks;
                            break;
                        case 3:
                            tehe = "jaga";
                            vastus = yks / kaks;
                            break;
                        default:
                            break;
                    }*/

                   Console.Write($"Leia arvude {yks} ja {kaks} {tehe}: "); // kirjutab lause tehte kohta, mida pead hakkama lahendama
                        for (int j = 0; j < 5; j++)
                {
                    if (int.Parse(Console.ReadLine()) == vastus) break;
                    Console.Write($"vale vastus{(j == 4 ? "\n" : " - proovi uuesti: ")}");
                    punkte--; // vale vastuse puhul võetakse 1 punkt maha
                }
                }

                // see ütleb, kuidas tulemus oli
            Console.WriteLine(
                punkte == 50 ? "suurepärane - kõik õiged vastused" :
                punkte > 40 ? "üsna hea tulemus" :
                punkte > 30 ? "päris hea tulemus" :
                punkte > 10 ? "kehv - harjuta veel" :
                "mine kooli tagasi"
                );
                }
            }
        }
     

