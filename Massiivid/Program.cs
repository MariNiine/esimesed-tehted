﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiivid
{
    class Program
    {
        static void Main(string[] args)
        {

            int i1 = 0;
            int i2 = 7;

            int[] arvud; // tegemist on massiiviga, aga tal pole hetkel sisu
            arvud = new int[i2]; // nüüd on massiivil sisu - ta koosneb 10st int-ist (nurksulgudes on avaldis, mitte number)
            

            for (int i = 0; i < arvud.Length; i++)
            {
                arvud[i] = i * i; // pöördumine indeksi kaudu
            }

            foreach (var x in arvud) ; //Console.WriteLine(x);


            // teeb sama, mis foreach aga keerulisemalt
            for (var e = arvud.GetEnumerator(); e.MoveNext();)
            {

                var x = e.Current;    Console.WriteLine(x);
            }

            int[] teisedArvud = new int[10]; // muutuja defineerimine KOOS väärtusega
            int[] kolmandadArvud = (int [])teisedArvud.Clone();
            kolmandadArvud[3] = 7;
           // Console.WriteLine(teisedArvud[3]);

            int[] veelYks = new int[5] { 1, 3, 6, 2, 7 }; // massiiv ja initializer
            veelYks = new int[] {7, 2, 3, 8, 9, 1, 0 };

            int[] viimane = { 1, 2, 3, 4, 5, 6, 7, 8 };

            // Mõned massiivid veel

            string[] kuud = { "Jan", "Feb", "March", "Apr", "May", "June" };

            int[][] paljuarve; // see on massiivide massiiv
            paljuarve = new int[3][];
            paljuarve[0] = new int []{ 1, 2, 3, 4 };
            paljuarve[1] = new int []{ 7,8,9 };
            paljuarve[2] = new int[] { 5, 6 };
           // Console.WriteLine(paljuarve[1][2]);

            int[,] tabel = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 0, 0, 10 } }; 
          //  Console.WriteLine(tabel[2,2]); // trükib välja 9 sest lugemine algas nullist, seega teise rea teine väärtus

            object[] suvaAsjad = { 1, "Mari", 2.7, new DateTime(1992, 03, 07) };

            paljuarve[1][2]++;
            suvaAsjad[0] = (int)(suvaAsjad[0]) + 1;
        }
    }
}
