﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Sport3
{
    class Program
    {
        static void Main(string[] args)
        {

            string filename = @"C:\Users\anton\Source\Repos\esimesed-tehted\Sport3\Sport3\TextFile1.txt";

            string[] read = File.ReadAllLines(filename); // tulemus massiivi ridadega asi

            string[] nimed = new string[read.Length - 1]; // miinus 1 selleks, et esimest rida ei loeks
            double[] kiirused = new double[read.Length - 1];
            double[] distants = new double[read.Length - 1];
    

            int kiireim = 0; // mitmes on kõige kiirem?  - las 1 on kõige kiirem . ilma nullita ei tööta, ei saa võrrelda kellegagi - siin muutujas kirjas, kes on kõige kiirem jooksja
            for (int i = 0; i < nimed.Length; i++) // käin läbi nimede massiivi, 
            {
                string[] tykid = read[i + 1].Replace(",", ",").Split(','); // ridades massiivist võtan rea
                nimed[i] = tykid[0]; // panen nimed nimede massiivi
                kiirused[i] = double.Parse(tykid[1]) / double.Parse(tykid[2]); // arvutan kiiruse välja, panen kiiruse massiivi
                Console.WriteLine($"{nimed[i]} kiirus {kiirused[i]:F2}");

                //kontrollin, kas praeguse rea kiirus on suurem kui meelde jäetud kiireima rea kiirus
                // praegune kiirus on kiirused[i]
                // meelde jäetud kõige suurem kiirus on kiirused[kiireim]
                if (kiirused[i] > kiirused[kiireim]) kiireim = i; // kiireim on indeks. Ei võrdle siin nulliga. Esimese paneme kirja, tuleb järgmine, võrdlen neid kiiruseid, eelmine oli kiire, järgmine oli aeglasem. Nüüd on 2 kõige kiirem. Iga kord võrreldakse sellleks hetkeks selgunud kõige kiiremaga.
            }
            Console.WriteLine($"Kõige kiirem on jooksja {nimed[kiireim]} kiirusega {kiirused[kiireim]:f2}");

            for (int i = 0; i < distants.Length; i++) // käin läbi distantsi massiivi
            {
                string[] tykid = read[i + 1].Replace(",", ",").Split(',');
                distants[i] = int.Parse(tykid[1]);
                Console.WriteLine(tykid[1]);

            }

            //otsin kiireimat distantsil
            int distants1 = 100;
            int distants2 = 200;
            int distants3 = 400;
            {
                for (int i = 0; i < distants.Length; i++)
                    if (distants[i] is 100) kiireim = i;
                Console.WriteLine($"100 m kõige kiirem on jooksja {nimed[kiireim]} kiirusega {kiirused[kiireim]:f2}");
                for (int i = 0; i < distants.Length; i++)
                    if (distants[i] is 200) kiireim = i;
                Console.WriteLine($"200 m kõige kiirem on jooksja {nimed[kiireim]} kiirusega {kiirused[kiireim]:f2}");
                for (int i = 0; i < distants.Length; i++)
                    if (distants[i] is 400) kiireim = i;
                Console.WriteLine($"400 m kõige kiirem on jooksja {nimed[kiireim]} kiirusega {kiirused[kiireim]:f2}");
            }


            //iga distantsi kiireim??

            for (int i = 0; i < 3; i++)

            {
                Console.Write("Millise distantsi kiireimat teada tahad?");
                switch (Console.ReadLine().ToLower())
                {
                    case "100":
                        Console.WriteLine("Kõige kiirem on Hurt ");
                        break;
                    case "200":
                        Console.WriteLine("Kõige kiirem on Janes ");
                        break;
                    case "400":
                        Console.WriteLine("Kõige kiirem on Janes ");
                        break;
                    default:
                        Console.WriteLine("Sellist dsitantsi pole");
                        break;
                }
            }
        }
    }
}
