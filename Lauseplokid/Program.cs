﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lauseplokid
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Kommentaarid
            //Protseduur või algoritm või tegevusjuhend koosneb lausetest
            // Kolm peamist lauset on 
            // 1. (muutuja) definitsioon;
            // 2. (muutujale uut väärtust) arvutav avaldis;
            // 3. meetodi väljakutse (ConsoleWriteline nt);

            // IF on lause ja ELVIS on avaldise tehe. Elvis arvutamise sees, IF tegutsemisel.  
            #endregion

            int age = 64; // muutuja, mis on kirjeldatud väljaspool plokki, kehtib ka allolevale
            {
                // See siin on lausete plokk. Muutuja, mis on kirjeldatud ploki sees {}, kehtib ainult ploki sees, väljapoole ei kehti. 
                string name = "Mari";
            }

            // if lause taha EI KÄI semikoolon

            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday) // tingimuslik lause. Boolean avaldis on peale if sulgude sees.
            {
                Console.WriteLine("Täna lähme sauna");
                int vihtadeArv = 7;
                Console.WriteLine($"Võtame kaasa {vihtadeArv} vihta");
                if (vihtadeArv == 0) Console.WriteLine("täna ei vihtle"); // see on äärmuslik juh
            }
            else
            {
                // need laused siin plokis täidetakse kui see IFitagune asi on false
            }

            // teeme ühe näite

            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            {
                //laupäevased laused
            }
            else if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
            {
                //siin on pühapäevased laused
            }
            else
            {
                // muud päevad
            }

            // MUUTUJA SKOOP
            string see = "See on protseduuri tasemel muutuja";
            {int kohalik = 5;}
            { string kohalik = "Kaheksa"; }

            //VALGUSFOOR
            // IF variant

            Console.Write("Mis värvi tuli on valgusfooris?");
            string varv = Console.ReadLine();

            if (varv == "Red")
            {Console.WriteLine("Jää seisma");}

            else if (varv == "Green")
            {Console.WriteLine("Mine edasi");}

            else if (varv == "Yellow")
            {Console.WriteLine("Oota");}

            else
            {Console.WriteLine("Pole sellist värvi");}
            
            //SWITCH variant

            Console.Write("Mis värvi tuli on valgusfooris?");
            switch (Console.ReadLine().ToLower())
            {
                case "green":
                    Console.WriteLine("Mine edasi");
                    break;
                case "yellow":
                    Console.WriteLine("Oota");
                    break;
                case "red":
                    Console.WriteLine("Ära mine");
                    break;
                default:
                    Console.WriteLine("Sellist värvi pole");
                    break; 
            }
            


        }
    }
}
