﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaardipakk
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //int[] pakk = new int[52];
            //int[] pakk = Enumerable.Range(0, 52).ToArray(); // samasugune massiiv
            List<int> pakk = Enumerable.Range(1, 52).ToList(); // samasugune list // sellisel kujul saan poest segamata kaardipaki
            Random r = new Random(); // kui tahan segada, siis vaja randomi kordajat

            // Esimene variant - ühest pakist teise
            Console.WriteLine("\n Pakk enne segamist \n");
            for (int i = 0; i < 52; i++)
            {
                Console.Write($"{pakk[i]:00}{(i % 13 == 12 ? "\n" : "\t")}");
            }

            List<int> segatudpakk = new List<int>(); // teen ühe paki
            while (pakk.Count > 0) // teen seda nii kaua kuni esimene pakk tühjaks saab
                {
                int mitmes = r.Next(pakk.Count); //leian juhusliku kaardi(number 0... kaartide arv)
                segatudpakk.Add(pakk[mitmes]);         //panen ka teise paki
                pakk.RemoveAt(mitmes);           //kustutan ta esimesest ära
                }

            Console.WriteLine("\n Pakk peale segamist \n");
            for (int i = 0; i < 52; i++)
            {
                Console.Write($"{segatudpakk[i]:00}{(i % 13 == 12 ? "\n" : "\t")}");
            }


            Console.WriteLine("\n TEINE VARIANT - UNIKAALSED KAARDID \n");

            segatudpakk = new List<int>();
            while(segatudpakk.Count < 52) 
                {
                int uus = r.Next(52);
                if (!segatudpakk.Contains(uus)) segatudpakk.Add(uus);
            }

            Console.WriteLine("\n Segatud pakk \n");
            for (int i = 0; i < 52; i++)
            {
                Console.Write($"{segatudpakk[i]:00}{(i % 13 == 12 ? "\n" : "\t")}");
            }

            /* KUIDAS VÕIKS SEDA ÜLESANNET LAHENDADA:
            1. Vaja on kaardipakki (mis ei ole segatud, muidu poleks ju midagi teha) - see etapp tee esimesena
             mis selle pidamiseks sobib? massiiv või list
             Segamata paki saan nt for tsükliga 
            
            2. Vaja on see segamini ajada
            
            3. On vaja välja trükkida (tee teisena)
             a) Üks variant on teha tsükkel (4 korda) ha sekkes teube (13 korda)
             b) Teha üks tsükkel aga iga 13 tagant vahetada rida
                 */
        }
    }
}
