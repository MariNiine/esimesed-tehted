﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esimesed_tehted
{
    class Program
    {
        static void Main(string[] args)
        {

            // tehted, mis on harjumatud ja millega tuleb harjuda
            // loogikatehe ja && ja või ||

            Console.WriteLine(5 & 6);  // mis trükitakse
            Console.WriteLine(5 | 6); //mis türkitakse

            //kuidas arvuti sisemisel arvudest aru saab
            //6 - 0110 6&5 0100 - tulemus 4
            //5 - 0101 6 | 5 0111 - tulemus 7
            //^ bitwise xor

            string nimi = "Mari Niinemäe";
            //Console.WriteLine(nimi [1]+0);

            string failinimi = @"C:\Users\marin_m3sdpn5\OneDrive\Documents\Visual Studio 2019"; // @ = unescaped string
            string jutt1 = "Luts jutustab: \" kui Arno jne jne\""; //jutumärk escapetud stringis
            string jutt2 = @"Luts jutustab: ""kui Arno jne jne"""; // jutumärk escapemata stringis topelt

            Console.WriteLine("Henn on ilus poiss \nja tark kahh"); // line feed ehk LF ehk CHAR (10)
            Console.WriteLine("Henn on ilus poiss \rAnts");         // carriage return ehk CR ehk char(13)

            //String on MUUTUMATU
            string nimi2 = "Mari";
            nimi2 += " ";
            nimi2 += "Niinemäe";
            Console.WriteLine(nimi);
            Console.WriteLine(nimi2);

            Console.WriteLine(nimi == nimi2 ? "samad" : "erinevad");       // Javas erinevad
            Console.WriteLine(nimi.Equals(nimi2, StringComparison.InvariantCultureIgnoreCase) ? "samad" : "erinevad"); // ka javas samad

            Console.Write("Tere "); // ei vaheta rida
            Console.WriteLine("Mari"); //teeb reavahetuse

            int arv1 = 7;
            decimal arv2 = 10.777M;

            // kõike nummerdatakse alates nullist

            // PLACEHOLDERID stringis (stringFormat)
            Console.WriteLine("arvud on sellised {0} ja {1:F2}", arv1, arv2); // F2 on format string - tekitab kaks kohta peale koma

            string tulemus = string.Format("arvud on sellised {0} ja {1:F2}", arv1, arv2);
            Console.WriteLine(tulemus);
            // PLACEHOLDERID avaldised - interpoleeritud string
            Console.WriteLine($"arvud on sellised {arv1} ja {arv2:F2}"); // $ - tegemist on interpoleeritud stringiga
            tulemus = $"arvud on sellised {arv1} ja {arv2:F2}";
            

            /*
            
            TEHETE PRIORITEEDID
            3 + 4 * 2 // korrutamine on prioriteetsem ja tehakse enne
            /%*
            +-
            & - loogiline korrutamine (seega see tehe enne)
            | - loogiline liitmine (see tehe peale & tehet)

            Aritmeetikatehted on vasakassotsiatiivsed ehk tehakse samal tasemel. Ehk tehakse vasakult paremale
            (avaldis1) + (avaldis2) + 7; Tehted tehakse vasakult paremale.

            Omistamistehe on paremassotsiatiivne
            a = b = c

            KASUTA SULGUSID, kui vaja olla kindel tehete järjekorras
            (avaldis1) * 7
            */
        }
    }
}
