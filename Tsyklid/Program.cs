﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tsyklid
{
    class Program
    {
        static void Main(string[] args)
        {
        
            //TSÜKLID - kui midagi on vaja teha mitu korda
            // esimene lauses defineeritakse muutuja int i = 0 või 
            // teine lause ütleb ei / jah kas jätkata
            // kolmas lause on jätkaja, kordaja - see lause täidetakse peale iga korda

            // FOR tsükkel
            // for ja 3 laused ehk for(initializer ehk alustaja; terminator ehk lõpetaja on alati avaldis; iterrator ehk kordaja) {blokk lausied, mda täidetakse mitu korda}

            //FOR TSÜKLI LOOGIKA
            // 1. täidetakse ära initializer;
            // 2. if (!terminator) lõpeta;
            // 3. blokk
            // 4 iterrator;
            // 5. mine tagasi teise punkti juurde

            for(int i = 0; i<10; i++)
            {
                Console.WriteLine($"Tee seda asja {i}-{(i == 1 || i == 2 ? "st" : "ndat")} korda");
                if (DateTime.Now.DayOfWeek == DayOfWeek.Friday) break;
            }


            //näide, et tsükli päises on võimalik kasutada suvalisi lauseid
            int vastus = 0;
            for (Console.Write("mis su palk on?");
            !int.TryParse(Console.ReadLine(), out vastus);
            Console.WriteLine("kirjuta ilusti")
            )
            { }
            { Console.WriteLine($"selge, su palk on {vastus}");}



            //WHILE tsükkel (boolean avaldis)
            //while (boolean-avaldis) {blokk lauseid}
            //WHILE tsükkes on sama mis FOR tsükkel lihtsalt ilma initializeri ja iterratorita

            // WHILE TSÜKLI LOOGIKA
            // 1. 
            // 2. if(!boolean-avaldis) lõpeta;
            // 3. blokk
            // 4. 
            // 5. mine tagasi tise punkti juurde

            for (int i = 0; i < 10; i++)
            { }
            Console.WriteLine("\n while tsüklikga\n");

            while (DateTime.Now.DayOfWeek == DayOfWeek.Friday)
            {
                Console.WriteLine("ootan reedet");
                System.Threading.Thread.Sleep(1000);
            }

            // DO tsükkel
            // do {blokk lauseid} while (boolean avaldis); 

            // DO TSÜKLI LOOGIKA
            // 1. blokk lauseid
            // 2. if (boolean-avaldis)

            Console.WriteLine("\n do tsüklikga\n");
            do
            {
                Console.WriteLine("ootan reedet");
                System.Threading.Thread.Sleep(1000);
            } while (DateTime.Now.DayOfWeek == DayOfWeek.Friday);

            // tsükli sees on kaks kasulikku lauset 
            // break; lõpetab tsükli täitmise ja jätab selle korduse pooleli
            // continue; läheb järgmisele kordusele (selle jätab pooleli)



        }
    }
}
